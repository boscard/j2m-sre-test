# J2M-SRE-TEST

CI setup for code originally published at https://github.com/psantori/j2m-sre-test

## Moving repository

To be able to easy test and build app using GitLab code should be stored in GitLab repository. This way CI pipeline can be triggered immediately after commits are pushed. There is possibility to clone GitHub repository periodically, but it is not ideal and adds some delays.

## CI Pipeline

### Stages

For this app I'm using three stages:
* test - used for all jobs testing application code
* build - used to compile code and prepare binary
* deploy - used as final step for creating and publishing docker container

### Jobs

#### format

This job is using `go fmt` and `go vet` to statically analyze app code and find issues which then can break compilation process.

#### tests

This job is executing `go test` tool to test various aspects of program:
* `go test` - executes implemented unit tests
* `go test -race` - checks code against possible race conditions
* `go test -coverity` - checks code coverity. This check can be used to with remembering about need of writing unit tests. Properly used will block MR when new functionality is introduced without proper unit tests

#### compile

This job is creating binary from source code. This binary is complete so it can be places in minimal container for distribution. During compilation we are also checking for possible race conditions.

#### publish

This step is creating docker image using binary from previous step an Dockerfile from this repository. Job is creating three tags:
* latest - standard tag added after docker image is created
* $CI_COMMIT_SHORT_SHA - this tag allows us to identify place in git tree from which image was build
* $CI_COMMIT_REG_SLUG - this tag is transformed branch name. It can be used for easy finding docker images created from branches or can be used for versions release

## Dockerfile

As base image I'm using minimal docker image which allows me to minimize size of final docker image. 

## Kubernetes manifests

Kubernetes manifests can be found in [examples/kubernetes](examples/kubernetes)