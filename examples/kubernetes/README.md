# J2M SRE Test

Example kubernetes manifests

```
$ kubectl apply -f redis.yaml 
deployment.apps/redis created
service/redis created
```
```
$ kubectl apply -f j2m-sre-test.yaml 
deployment.apps/j2m-sre-test created
service/j2m-sre-test created
```
```
$ kubectl port-forward svc/j2m-sre-test 8080 &
[1] 37506
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
```
```
$ curl localhost:8080
Handling connection for 8080
Hello, visitor number 1!
$ curl localhost:8080
Handling connection for 8080
Hello, visitor number 2!
```