FROM gcr.io/distroless/base-debian10

WORKDIR /
ENV REDIS_URL=redis:6379
ENV SERVICE_ADDR=0.0.0.0:8080
EXPOSE 8080
USER nonroot:nonroot
ENTRYPOINT ["/app"]

COPY app /app
